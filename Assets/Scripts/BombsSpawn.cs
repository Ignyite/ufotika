﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombsSpawn : MonoBehaviour
{
    [SerializeField] private GameObject bombsPrefab;
    [SerializeField] private List<GameObject> pointSpawn=new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            int place=Random.Range(0, pointSpawn.Count);
            Instantiate(bombsPrefab, pointSpawn[place].transform.position, Quaternion.identity);
            pointSpawn.RemoveAt(place);
            
        }
    }
}

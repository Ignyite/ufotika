﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float distanceMoving=0.8f;
    private Vector3? previosPosition;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetMouseButtonDown(0))
        {
            previosPosition = GetWorldPoint();
            return;
        }

        if (!Input.GetMouseButton(0))
        {
            return;
        }
        if(previosPosition==null)
        {
            return;
        }
        
        var pos = GetWorldPoint();
        var dir = previosPosition.Value - pos.Value;
        dir.y = 0;
        dir.x = Mathf.Clamp(dir.x, -distanceMoving, distanceMoving);
        dir.z = Mathf.Clamp(dir.z, -distanceMoving, distanceMoving);
        transform.Translate(dir,Space.World);
        Debug.Log(dir.magnitude);
        previosPosition = GetWorldPoint();
    }

    private Vector3? GetWorldPoint()
    {
        var mousePos = Input.mousePosition;
        var worldPoint = Camera.main.ScreenToWorldPoint(mousePos);
        var direction = Camera.main.transform.forward;
        RaycastHit hit;
        if (Physics.Raycast(worldPoint, direction, out hit))
        {
            return hit.point;
        }
        return worldPoint;
    }
}

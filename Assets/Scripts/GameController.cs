﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//не мое
public class GameController : MonoBehaviour
{
    public int objectsToPickUp;
    public int pickUpingObjects=0; 
    public int ScoreNum;
    int time = 300;
    [SerializeField] GameObject loseSprite;
    [SerializeField] GameObject winSprite;
    [SerializeField] GameObject barSprite;
    [SerializeField] GameObject playSprite;
    [SerializeField] GameObject tutorial;

    [SerializeField]
    private float musicVolume=0.1f;
    [SerializeField] private AudioSource winAuidio;
    [SerializeField] private AudioSource failAudio;

    private List<GameObject> listPickUpObjects;
    private void Awake()
    {
        listPickUpObjects=new List<GameObject>();
        listPickUpObjects.AddRange(GameObject.FindGameObjectsWithTag("object"));
        objectsToPickUp = listPickUpObjects.Count;
        float sum = 0;
        foreach (var obj in listPickUpObjects)
        {
            if (obj.GetComponent<PriceObject>() == null)
            {
                print(obj.name);
            }
            sum += obj.GetComponent<PriceObject>().price;
        }
        print(sum);
        print(objectsToPickUp);
        PlayerPrefs.SetFloat("MusicVolume",0.1f);
    }

    void Start()
    {
        
        Time.timeScale = 1;
        
        playSprite.SetActive(true);

        if (!PlayerPrefs.HasKey("first"))
        {
            PlayerPrefs.SetInt("first", -1);
            playSprite.SetActive(false);
            Invoke("InitTutorial", 1.5f);

        }

    }

    // Update is called once per frame
    void Update()
    {
        // if (pickUpingObjects >= objectsToPickUp * 0.9)
        // {
        //     Win();
        // }
    }



    public void Win()
    {
        winAuidio.Play();
        winAuidio.volume = 0.1f;
        winSprite.SetActive(true);
        barSprite.SetActive(false);
        gameObject.transform.GetChild(0).gameObject.SetActive(false); //ufo off
        Time.timeScale = 0;

    }

    public void Lose()
    {
        failAudio.Play();
        failAudio.volume = 0.1f;
        loseSprite.SetActive(true);
        barSprite.SetActive(false);
        gameObject.transform.GetChild(0).gameObject.SetActive(false); //ufo off
        Time.timeScale = 0;

    }

    void InitTutorial()
    {
        tutorial.SetActive(true);
    }


    /*public IEnumerator TikTak()
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(1);
            time--;


            //barSprite.GetComponent<RectTransform>().sizeDelta = new Vector2(Mathf.Lerp(barSprite.GetComponent<RectTransform>().sizeDelta.x, 0, 2 * Time.deltaTime), 40); //x: 600>0

            if (time <= 0)
            {
                Lose();
                break;
            }
        }*/
        

}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    // [SerializeField] private float speed=0.2f;
    // [SerializeField] Vector2 RightLeft, UpDown;
    // [SerializeField] private Camera mainCamera;
    
    [SerializeField] private Vector3 minPos;
    [SerializeField] private Vector3 maxPos;

    [SerializeField] private float dragDistance=0.05f;
    // public Transform transformUfo;
    // private Transform cameraTransform;
    // private Vector3 startInputPos;

    private Vector3 angleFix =new Vector3( 0f, 135f, 0f);
    private GameObject obj;
    // Start is called before the first frame update
    void Start()
    {
       
        //cameraTransform = transform;
        //StartCoroutine(Move());
        //cameraTransform.parent = obj.transform;
        
    }

    [SerializeField] private float dragSpeed = 0.05f;
    private Vector3 dragOrigin;
 
 
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }
 
        if (!Input.GetMouseButton(0)) return;
 
        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);
        move.x=Mathf.Clamp(move.x, -dragDistance, dragDistance);
        move.z=Mathf.Clamp(move.z, -dragDistance*2, dragDistance*2);
        transform.Translate(move, Space.World);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minPos.x, maxPos.x), transform.position.y,Mathf.Clamp(transform.position.z, minPos.z, maxPos.z) );
    }
   
}

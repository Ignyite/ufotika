﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
//не мое

public class PickUpObject : MonoBehaviour
{
    [SerializeField] private GameController gameController;
    [SerializeField] private float timePickUp = 6f;
    [SerializeField] private float radiusPickUp;
    public float musicVolume = 0.10f;
    [SerializeField] Text ScoreTxt;
    [SerializeField] RectTransform barScore;
    [SerializeField] private Slider slider;
    [SerializeField] private Text winText;
    [SerializeField] private Text loseText;
    [SerializeField] private Text winMaxScore;
    [SerializeField] private Text loseMaxScore;
    [SerializeField] private AudioSource eating;
    [SerializeField] private float maxScore = 0;
    
    private float counter = 0;
    private void Start()
    {
        slider.maxValue = 1043;
        if (!PlayerPrefs.HasKey("MaxScore"))
            PlayerPrefs.SetInt("MaxScore", 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        
       
        if (other.gameObject.tag == "object" ||other.gameObject.tag == "bomba")
        {
            counter++;
            if (other.gameObject.GetComponent<AudioSource>() != null)
            {
                if (other.gameObject.tag == "bomba")
                {
                    other.gameObject.GetComponent<AudioSource>().PlayDelayed(1f);
                    other.gameObject.GetComponent<AudioSource>().volume = musicVolume;
                }
                else
                {
                    if (counter % 5 == 0)
                    {
                        eating.Play();
                        
                    }
                    other.gameObject.GetComponent<AudioSource>().Play();
                    other.gameObject.GetComponent<AudioSource>().volume = other.gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("MusicVolume");
                    
                }
              
            }




            //other.transform.Rotate(new Vector3(Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f)),Space.World);
            StartCoroutine(PickUp(other.transform));
         
        }
    }


    private  IEnumerator PickUp(Transform other)
    {
        float timer = 0f;
      
        var initPosition = other.position;
        var initRotation = other.rotation;
        var randRotation = Random.rotation;
        while (timer<1f)
        {
          
            timer += Time.deltaTime/timePickUp;
            if (timer > 1)
                timer = 1;
            if (other != null)
            {
                other.rotation = Quaternion.Slerp(initRotation, randRotation, timer);
                other.position=Vector3.Lerp(initPosition,new Vector3(transform.position.x,initPosition.y,transform.position.z), 0.9f);
                other.position=Vector3.Lerp(initPosition,transform.position,timer);
                
            }
            
            if (Vector3.Distance(other.position, transform.position) < 0.2f)
            {
           
                if (other.gameObject.tag == "bomba")
                {
                    gameObject.GetComponentInParent<GameController>().Lose();
                    loseText.text = gameController.ScoreNum.ToString();
                    if (PlayerPrefs.GetInt("MaxScore") < gameController.ScoreNum)
                    {
                        PlayerPrefs.SetInt("MaxScore",gameController.ScoreNum);
                        loseMaxScore.text = gameController.ScoreNum.ToString();
                    }

                    loseMaxScore.text = PlayerPrefs.GetInt("MaxScore").ToString();
                    break;
                }
                   
                gameController.pickUpingObjects++;
                slider.value += other.gameObject.GetComponent<PriceObject>().price;
                if (gameController.pickUpingObjects >= gameController.objectsToPickUp * 0.9)
                {
                    gameObject.GetComponentInParent<GameController>().Win();
                    winText.text = gameController.ScoreNum.ToString();
                    if (PlayerPrefs.GetInt("MaxScore") < gameController.ScoreNum)
                    {
                        PlayerPrefs.SetInt("MaxScore",gameController.ScoreNum);
                        winMaxScore.text = gameController.ScoreNum.ToString();
                    }

                    winMaxScore.text = PlayerPrefs.GetInt("MaxScore").ToString();
                    break;
                }
                print(gameController.pickUpingObjects);
                float bX = barScore.sizeDelta.x;
                barScore.sizeDelta = new Vector2(bX + 6.81f, 40);  //98 Объектов, 100%=600 по х, 1% = 6,12
                // if (slider.value == slider.maxValue)
                // {
                //     gameObject.GetComponentInParent<GameController>().Win();
                //     break;
                // }
                //if (bX > 599) { gameObject.GetComponentInParent<GameController>().Win(); } //100% победа

               

                GameController GC = gameObject.GetComponentInParent<GameController>();
                GC.ScoreNum += (int)other.gameObject.GetComponent<PriceObject>().price;
                ScoreTxt.text = GC.ScoreNum.ToString();

                Destroy(other.gameObject);
                break;
            }
            yield return null;

        }
        
    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateUfo : MonoBehaviour
{
    [SerializeField] private float angleSpeed = 15f;
    [SerializeField] private float angleZRotation = 7f;
    private Transform trans;
    // Start is called before the first frame update
    void Start()
    {
        trans = transform;
    }

    // Update is called once per frame
    void Update()
    {
        trans.RotateAround(trans.position,Vector3.up, angleSpeed*Time.deltaTime);
        if(transform.rotation.z<= trans.rotation.z-angleZRotation)
            trans.RotateAround(trans.position,Vector3.forward, angleZRotation*Time.deltaTime);
        if(transform.rotation.z> trans.rotation.z+angleZRotation)
            trans.RotateAround(trans.position,Vector3.forward, -angleZRotation*Time.deltaTime);
    }
    
}

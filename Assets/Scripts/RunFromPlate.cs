﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunFromPlate : MonoBehaviour
{
    [SerializeField] private Transform ufo;

    [SerializeField] private float speed = 2f;
    
    // Start is called before the first frame update
    private void Update()
    {
        var heading = ufo.position - transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance;
        transform.Translate(speed*Time.deltaTime*new Vector3(-direction.x,0,-direction.z),Space.World);
    }
}

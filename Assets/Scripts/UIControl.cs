﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIControl : MonoBehaviour
{
    [SerializeField] GameObject PlaySprite;
    [SerializeField] GameObject BarScore;
    [SerializeField] GameObject TutorialObj;
    [SerializeField] private GameObject buttonPlay;

    public void Play()
    {
        PlaySprite.SetActive(false);
        BarScore.SetActive(true);
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.transform.GetChild(1).gameObject.SetActive(true);
        //TutorialObj.SetActive(true);
        //ufo включить меш
        gameObject.GetComponent<AudioSource>().Play();
        gameObject.GetComponent<AudioSource>().volume = 0.1f;
//        gameObject.transform.GetComponent<CameraMove>().enabled = true;
    }


    public void Again()
    {
        SceneManager.LoadScene(0);    
       // Play();
    }

    public void Home()
    {
        SceneManager.LoadScene(0);
    }

    public void EndTutorial()
    {
        TutorialObj.SetActive(false);
        PlaySprite.SetActive(true);
    }


}
